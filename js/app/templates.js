import { getTextareaAlignment } from "./alignement.js";
import { getCurrentTime } from "./utils.js";

export function renderTemplate($message) {
  return ($template, content) => {
    const alignment = getTextareaAlignment($message);

    $("div.msg-content", $template).text(content);
    $("small.text-primary", $template).text(getCurrentTime());
    $template.addClass(alignment);

    $("#dialog").find("div.row").append($template);
    $message.val("");
  };
}
export function getTemplate(name) {
  return $.get(`templates/${name}.html`).then($);
}
