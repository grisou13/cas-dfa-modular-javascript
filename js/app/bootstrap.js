import { changeAlignment } from "./alignement.js";
import { switchListItem } from "./dialogs.js";
import { createNewMessage, removeMessage } from "./messages.js";

const $alignButtons = $("#align-btns");
const $message = $("#message");
//I changed functions to be bootstraped

$("#send-btn").on("click", createNewMessage($message));
$("#dialog").on("click", "button", removeMessage);
$("a.list-group-item").on("click", switchListItem);
$("button", $alignButtons).on("click", changeAlignment($alignButtons));
