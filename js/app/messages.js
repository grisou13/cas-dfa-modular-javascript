import { getTemplate, renderTemplate } from "./templates.js";

export function createNewMessage($message) {
  const render = renderTemplate($message);
  return (event) => {
    const msgValue = $message.val();
    if (msgValue === "") {
      $message.addClass("is-invalid");
    } else {
      getTemplate("new-message")
        .then($)
        .then(($template) => render($template, msgValue));
    }
    event.preventDefault();
  };
}
export function removeMessage(event) {
  $(event.currentTarget).closest("div.col-8").remove();
}
